﻿using GymReact.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.DAL
{
    public class GymReactContext : DbContext
    {
        public DbSet<User> users;
        public DbSet<Profile> profiles;
        public DbSet<Address> addresses;
        public DbSet<Exercise> exercises;
        public DbSet<Goal> goals;
        public DbSet<GymReact.Models.Program> programs;
        public DbSet<ProgramWorkout> programWorkouts;
        public DbSet<Set> sets;
        public DbSet<Workout> workouts;

        public GymReactContext(DbContextOptions<GymReactContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<GoalWorkout>().HasKey(gw => new { gw.GoalId, gw.WorkoutId });
            modelBuilder.Entity<ProgramWorkout>().HasKey(pw => new { pw.ProgramId, pw.WorkoutId });
            modelBuilder.Entity("GymReact.Models.ProgramWorkout", b =>
            {
                b.HasOne("Workout").WithMany().HasForeignKey("WorkoutId").OnDelete(DeleteBehavior.NoAction);
            });
            modelBuilder.Entity("GymReact.Models.GoalWorkout", b =>
            {
                b.HasOne("Workout").WithMany().HasForeignKey("WorkoutId").OnDelete(DeleteBehavior.NoAction);
            });



        }
        public DbSet<GymReact.Models.Profile> Profile { get; set; }
        public DbSet<GymReact.Models.User> User { get; set; }
        public DbSet<GymReact.Models.Address> Address { get; set; }
        public DbSet<GymReact.Models.Exercise> Exercise { get; set; }
        public DbSet<GymReact.Models.Workout> Workout { get; set; }
        public DbSet<GymReact.Models.Set> Set { get; set; }
        public DbSet<GymReact.Models.Goal> Goal
        {
            get; set;
        }
    }
}
