# gym-react

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
> Gym React Assignment

## Table of Contents

- [Run](#run)
- [Maintainers](#maintainers)
- [Components](#components)
- [Contributing](#contributing)
- [License](#license)

## Run
```
Need Entity Framework SQLServer, Tools, Proxies and InMemory
Use NGPM to update-database
Visual Studio Run
```
## Components

Class Library with Controllers in C#
Fetch-data and AddProfile components uses ProfilesController, UsersController and AddressesController to add to the DB

Fetch-exercise and AddExercise components uses ExerciseController, SetsController and WorkoutsController to add to the DB

AddGoal component uses GoalsController to add to the DB

## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS