﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GymReact.Migrations
{
    public partial class bigmig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Set_Profile_ProfileId",
                table: "Set");


            migrationBuilder.AddForeignKey(
                name: "FK_Set_Profile_ProfileId",
                table: "Set",
                column: "ProfileId",
                principalTable: "Profile",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Set_Profile_ProfileId",
                table: "Set");

            migrationBuilder.DropForeignKey(
                name: "FK_Set_Profile_ProfileId1",
                table: "Set");

            migrationBuilder.DropIndex(
                name: "IX_Set_ProfileId1",
                table: "Set");

            migrationBuilder.DropColumn(
                name: "ProfileId1",
                table: "Set");

            migrationBuilder.AddForeignKey(
                name: "FK_Set_Profile_ProfileId",
                table: "Set",
                column: "ProfileId",
                principalTable: "Profile",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
