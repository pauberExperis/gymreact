﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class GoalWorkout
    {
        public DateTime EndDate { get; set; }
        public virtual Goal Goal { get; set; }
        public int GoalId { get; set; }
        public virtual Workout Workout { get; set; }
        public int WorkoutId { get; set; }
    }
}
