﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class Set
    {
        public int Id { get; set; }
        public int ExerciseRepetitions { get; set; }
        public virtual Exercise Exercise { get; set; }
        public int ExerciseId { get; set; }
        public virtual Profile? Profile { get; set; }
        public int? ProfileId { get; set; }
        public virtual Workout? Workout { get; set; }
        public int? WorkoutId { get; set; }
    }
}
