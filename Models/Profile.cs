﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public virtual User User { get; set; }
        public int UserId { get; set; }
        public virtual Address Address { get; set; }
        public virtual ICollection<Set>? Sets {get; set;}
        public virtual ICollection<Workout>? Workouts { get; set; }
        public virtual GymReact.Models.Program? Program { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; } 
        public virtual Goal Goal { get; set; }
        public string? MedicalConditions { get; set; }
        public string? Disabilities { get; set; }
    }
}
