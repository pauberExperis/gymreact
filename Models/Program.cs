﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class Program
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public virtual Profile Profile { get; set; }
        public int ProfileId { get; set; }
        public virtual ICollection<Goal> Goals { get; set; }
        public virtual ICollection<ProgramWorkout> programWorkouts { get; set; }
    }
}
