﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class ProgramWorkout
    {
        public virtual Workout Workout { get; set; }
        public int WorkoutId { get; set; }
        public virtual GymReact.Models.Program Program { get; set; }
        public int ProgramId { get; set; }
    }
}
