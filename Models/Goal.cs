﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class Goal
    {
        public int Id { get; set; }
        public DateTime EndDate { get; set; }
        public string Name { get; set; }
        public bool Achieved { get; set; }
        public virtual Program? Program { get; set; }
        public int? ProgramId { get; set; }
        public virtual Profile Profile { get; set; }
        public int ProfileId { get; set; }
        public virtual ICollection<GoalWorkout> goalWorkouts { get; set; }
    }
}
