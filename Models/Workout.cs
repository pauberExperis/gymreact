﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class Workout
    {
        public int Id { get; set; }
        public string Name { get; set;  }
        public string Type { get; set; }
        public bool Complete { get; set; } = false;
        public virtual ICollection<Set>? Sets { get; set; }
        public virtual Profile Profile { get; set; }
        public int ProfileId { get; set; }
        public virtual ICollection<ProgramWorkout> programWorkouts { get; set; }
        public virtual ICollection<GoalWorkout> goalWorkouts { get; set; }
    }
}
