﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymReact.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public bool IsContributor { get; set; }
        public bool IsAdmin { get; set; }

        public virtual Profile Profile { get; set; }

    }
}
