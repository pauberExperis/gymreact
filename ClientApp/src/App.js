import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { AddProfile } from './components/AddProfile';
import { AddExercise } from './components/AddExercise';
import { FetchExercise } from './components/FetchExercise';
import { AddGoal } from './components/AddGoal';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
            <Route path='/fetch-data' component={FetchData} />
            <Route path='/add-profile' component={AddProfile} />
            <Route path='/add-exercise' component={AddExercise} />
            <Route path='/fetch-exercise' component={FetchExercise} />
            <Route path='/add-goal' component={AddGoal}/>
      </Layout>
    );
  }
}
