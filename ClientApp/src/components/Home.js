import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>Hello, gym goers!</h1>
        <p>Register profiles and register exercises to the profiles as you see fit.</p>
      </div>
    );
  }
}
