﻿import React, { Component } from 'react';
import { withRouter } from "react-router-dom";

export class AddProfile extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            fname: "", lname: "", pwd: "", address1: "", address2: "", address3: "", post: "", city: "",
            country: "", height: 0, weight: 0, medical: "", disabilities: "", prof: {}, user: {}, address: {}
        };
        this.addProfile = this.addProfile.bind(this);
        this.addUser = this.addUser.bind(this);
        this.addAddress = this.addAddress.bind(this);
    }

    async addUser(event) {
        event.preventDefault();
        let that = this.state;
        let user = { Password: that.pwd, First_Name: that.fname, Last_Name: that.lname, IsContributor: false, IsAdmin: false }

        fetch('api/users', {
            method: 'POST',
            body: JSON.stringify(user),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => {
            console.log(response); return response.json();
        }).then(json => {
            this.setState({
                user: json
            });
        }).then(s => this.addProfile());
    }


    async addAddress() {
            let that = this.state;
            let address = { ProfileId: this.state.prof.id, AddressLine1: that.address1, AddressLine2: that.address2, AddressLine3: that.address3, PostalCode: that.post, City: that.city, Country: that.country }
        fetch('api/addresses', {
            method: 'POST',
            body: JSON.stringify(address),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => {
            return response.json()
        }).then(json => {
            this.setState({
                address: json
            });
        }).then(s => { this.props.history.push("/fetch-data")});
        }
    
    async addProfile() {
            let that = this.state;
            let prof = {
                UserId: this.state.user.id,
                Weight: that.weight, Height: that.height, MedicalConditions: that.medical, Disabilities: that.disabilities
            };
            fetch('api/profiles', {
                method: 'POST',
                body: JSON.stringify(prof),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            }).then(response => {
                return response.json()
            }).then(res => {
                this.setState({
                    prof: res
                });
            }).then(s=> this.addAddress());
        }
            
    
    /*updateFName(evt) {
        this.state = { fname: evt.target.value };
    }
    updateLName(evt) {
        this.state = { lname: evt.target.value };
    }
    updateAddress1(evt) {
        this.state = { address1: evt.target.value };
    }
    updateAddress2(evt) {
        this.state = { address2: evt.target.value };
    }
    updateAddress3(evt) {
        this.state = { address3: evt.target.value };
    }
    updatePost(evt) {
        this.state = { post: evt.target.value };
    }
    updateCity(evt) {
        this.state = { city: evt.target.value };
    }
    updateCountry(evt) {
        this.state = { country: evt.target.value };
    }
    updateHeight(evt) {
        this.state = { height: evt.target.value };
    }
    updateWeight(evt) {
        this.state = { weight: evt.target.value };
    }
    updateMedical(evt) {
        this.state = { medical: evt.target.value };
    }
    updateDisabilities(evt) {
        this.state = { disabilities: evt.target.value };
    }*/

    render() {
        return (
            <div style={{ textAlign: "center" }}>
                <h1>Add Profile</h1>
                <div style={{ textAlign: "center" }}>
                    <form onSubmit={this.addUser}>
                    <label htmlFor="fname">First name:
                    <input onChange={event => this.setState({ fname: event.target.value })} type="text" id="fname" name="fname" required/><br/><br/></label> 
                    <label htmlFor="lname">Last name:
                    <input onChange={event => this.setState({ lname: event.target.value })} type="text" id="lname" name="lname" required /><br /><br /> </label>
                        <label htmlFor="pwd">Password:
                    <input onChange={event => this.setState({ pwd: event.target.value })} type="password" id="pwd" name="pwd" pattern=".{8,}" title="Eight or more characters"
                                required /><br /><br /> </label>
                    <label htmlFor="address1"> Address Line 1:
                    <input onChange={event => this.setState({ address1: event.target.value })} type="text" id="address1" name="address1" required /> <br /><br /> </label>
                    <label htmlFor="address2"> Address Line 2:
                    <input onChange={event => this.setState({ address2: event.target.value })} type="text" id="address2" name="address2" /> <br /><br /> </label>
                    <label htmlFor="address3"> Address Line 3:
                    <input onChange={event => this.setState({ address3: event.target.value })} type="text" id="address2" name="address3" /> <br /><br /> </label>
                    <label htmlFor="post"> Post number: 
                    <input onChange={event => this.setState({ post: event.target.value })} type="text" id="post" name="post" pattern="[0-9]{4}" title="Valid postal code" required/> <br /><br /></label>
                    <label htmlFor="city"> City: 
                    <input onChange={event => this.setState({ city: event.target.value })} type="text" id="city" name="city" required/> <br /><br /></label>
                    <label htmlFor="country"> Country: 
                    <input onChange={event => this.setState({ country: event.target.value })} type="text" id="country" name="country" required /> <br /><br /></label>
                    <label htmlFor="height"> Height: 
                    <input onChange={event => this.setState({ height: event.target.value })} type="text" id="height" name="height" pattern="^[0-9]*" title="put height in numbers" required/> <br /><br /></label>
                    <label htmlFor="weight"> Weight: 
                    <input onChange={event => this.setState({ weight: event.target.value })} type="text" id="weight" name="weight" pattern="^[0-9]*" title="put weight in numbers" required/> <br /><br /></label>
                    <label htmlFor="medical"> Medical Conditions: 
                    <input onChange={event => this.setState({ medical: event.target.value })} type="text" id="medical" name="medical" /> <br /><br /></label>
                    <label htmlFor="disabilities"> Disabilities: 
                    <input onChange={event => this.setState({ disabilities: event.target.value })} type="text" id="disabilities" name="disabilities" /> <br /><br /></label>
                        <input type="submit" value="Submit"/>
                    </form>
                 </div>

                
            </div>
        );
    }
}

export default withRouter( AddProfile );
