﻿import React, { Component } from 'react';
import { withRouter } from "react-router-dom";

export class AddGoal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "", endDate: new Date().toLocaleString, achieved: false, profileId: 0, profile: {}, goal: {}, profiles: [], loading: true
        };
        this.addGoal = this.addGoal.bind(this);
    }
    componentDidMount() {
        this.populateProfileData();
    }


    /*async addSet() {
        let that = this.state;
        let set = {
            ExerciseRepetitions: that.exerciseRepetitions, ProfileId: that.profileId, WorkoutId: that.workout.id,
            ExerciseId: that.exercise.id
        }

        fetch('api/sets', {
            method: 'POST',
            body: JSON.stringify(set),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => {
            console.log(response); return response.json();
        }).then(json => {
            console.log(json);
            this.setState({
                set: json
            });
        }).then(s => { this.props.history.push("/") });
    }


    async addWorkout() {
        let that = this.state;
        console.log(that.profileId);
        let workout = {
            Name: that.workoutName, Type: that.workoutType, Complete: that.complete,
            ProfileId: that.profileId
        }
        fetch('api/workouts', {
            method: 'POST',
            body: JSON.stringify(workout),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => {
            return response.json()
        }).then(json => {
            console.log(json);
            this.setState({
                workout: json
            });
        }).then(s => this.addSet());
    }*/

    async addGoal(event) {
        
            event.preventDefault();
            let that = this.state;
            let goal = {
                Name: that.name, EndDate: that.endDate, Achieved: that.achieved, ProfileId: that.profileId
        };
        console.log(that.profileId)
        if (that.profileId != -1) {
            console.log(that.profile);
            fetch('api/goals', {
                method: 'POST',
                body: JSON.stringify(goal),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            }).then(response => {
                return response.json()
            }).then(res => {
                console.log(res);
                this.setState({
                    goal: res
                });
            }).then(s => { this.props.history.push("/") });
        }
            
        
        
        
    }


    render() {
        if (!this.state.loading) {
            return (
                <div style={{ textAlign: "center" }}>
                    <h1>Add Goal</h1>
                    <div style={{ textAlign: "center" }}>
                        <form onSubmit={this.addGoal}>
                            <label htmlFor="name">Goal Name:
                    <input onChange={event => this.setState({ name: event.target.value })} type="text" id="name" name="name" required /><br /><br /></label>
                            <label htmlFor="end">End Date:
                    <input onChange={event => this.setState({ endDate: event.target.value })} type="date" id="end" name="end" required /><br /><br /> </label>
                            <label htmlFor="profile"> Profile:
                                <select id="profile" required onChange={event => this.setState({ profileId: event.target.value })}>
                                    <option key="choose" value={-1}>Select Profile</option>
                                    {this.state.profiles.map((profile) => {
                                        if (profile.goal == null) {
                                            return (<option key={profile.id} value={profile.id} > {profile.user.first_Name + ' ' + profile.user.last_Name}</option>
                                            )
                                        }
                                    })}</select></label>
                            <input type="submit" value="Submit" />
                        </form>
                    </div>


                </div>
            )
        } else {
            return (
                <div> Loading...</div>);
        }
    }
    async populateProfileData() {
        const response = await fetch('api/profiles');
        const data = await response.json();
        this.setState({ profiles: data, profileId: -1, profile: data[0], loading: false });
    }
}

export default withRouter(AddGoal);
