﻿import React, { Component } from 'react';

export class FetchExercise extends Component {
    static displayName = FetchExercise.name;

    constructor(props) {
        super(props);
        this.state = {
            exercises: [], loading: true, exercise: {}, isUpdated: false
        };
        this.deleteExercise = this.deleteExercise.bind(this);
        this.renderExercisesTable = this.renderExercisesTable.bind(this);
        this.updateExercise = this.updateExercise.bind(this);

    }

    componentDidMount() {
        this.populateExerciseData();
    }


    renderExercisesTable(exercises) {
        console.log(exercises);
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Target Muscle Group</th>
                        <th>Exercise Repetitions</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody>
                    {exercises.map(exercise =>
                        <tr key={exercise.id}>
                            <td>{exercise.name}</td>
                            <td><input type="text" defaultValue={exercise.description}
                                onChange={(event) => exercise.weight = event.target.value}/></td>
                            <td> <input type="text"  defaultValue={exercise.targetMuscleGroup} onChange={(event) =>
                                exercise.targetMuscleGroup = event.target.value} /></td>
                            <td><input type="number" pattern="[0-9]*" defaultValue={exercise.set.exerciseRepetitions} onChange={(event) =>
                                exercise.set.exerciseRepetitions = (event.target.validity.valid) ? parseInt(event.target.value) : exercise.set.exerciseRepetitions} /></td>
                            <td>{exercise.set.profile.user.first_Name} {exercise.set.profile.user.last_Name}</td>
                            <td>
                                <button className="submit" onClick={() => this.updateExercise(exercise)} value={exercise.id}><b>✓</b></button><span> </span>
                                <button className="round" onClick={this.deleteExercise} value={exercise.id}>X</button></td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderExercisesTable(this.state.exercises);

        return (
            <div>
                <h1 id="tabelLabel" >Exercises</h1>
                {this.state.isUpdated ? <h2 className="updated"> Updated! </h2> : null}
                {contents}
            </div>
        );
    }

    async populateExerciseData() {
        const response = await fetch('api/exercises');
        const data = await response.json();
        this.setState({ exercises: data, loading: false });
    }
    deleteExercise(event) {
        fetch('api/exercises/' + event.target.value, {
            method: 'DELETE',
        }).then(res => res.text())
            .then(res => console.log(res)).then(s => {
                this.setState({ isUpdated: false }); this.componentDidMount();
            });

    }
    updateExercise(exercise) {
        console.log(exercise);
        let exer = {
            id: exercise.id, name: exercise.name, description: exercise.description, targetMuscleGroup: exercise.targetMuscleGroup, image: exercise.image,
            vidlink: exercise.vidlink
        };
        fetch('api/exercises/' + exer.id, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(exer)
        }).then(s => {
            this.setState({ isUpdated: true }); this.componentDidMount();
        });
    }
}
