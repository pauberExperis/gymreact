﻿import React, { Component } from 'react';

export class FetchData extends Component {
  static displayName = FetchData.name;

  constructor(props) {
    super(props);
      this.state = {
          profiles: [], loading: true, profile: {}, isUpdated: false, isDeleted: false};
      this.deleteProfile = this.deleteProfile.bind(this);
      this.renderProfilesTable = this.renderProfilesTable.bind(this);
      this.updateProfile = this.updateProfile.bind(this);

  }

  componentDidMount() {
    this.populateProfileData();
    }
    

    renderProfilesTable(profiles) {
        console.log(profiles);
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Weight</th>
                        <th>Height</th>
                        <th>Goal</th>
                        {//<th>Goal Achieved</th>
                        }
          </tr>
        </thead>
        <tbody>
          {profiles.map(profile =>
            <tr key={profile.id}>
                  <td>{profile.user.first_Name + ' ' + profile.user.last_Name}</td>
                  <td>{profile.address.addressLine1} {profile.address.addressLine2} {profile.address.addressLine3}
                      , {profile.address.postalCode} {profile.address.city}</td>
                  <td><input type="text" pattern="[0-9]*" defaultValue={profile.weight} onChange={(event) =>
                      profile.weight = (event.target.validity.valid) ? parseInt(event.target.value) : profile.weight} /> kg</td>
                  <td><input type="text" pattern="[0-9]*" defaultValue={profile.height} onChange={(event) =>
                      profile.height = (event.target.validity.valid) ? parseInt(event.target.value) : profile.height} />cm</td>
                  <td>{(() => {
                      if (profile.goal != null) {
                          return profile.goal.name;
                      } else {return "None"}
                  })()}</td>
                  {/*<td>{(() => {
                      if (profile.goal != null) {
                          if (profile.goal.achieved) {
                              return "Yes";
                          } else {
                              return "No";
                          }
                      } else { return "No" }
                  })()}</td>*/}
                  <td>
                      <button className="submit" onClick={() => this.updateProfile(profile)} value={profile.id}><b>✓</b></button><span> </span>
                      <button className="round" onClick={() => this.deleteProfile(profile)} value={[profile.user.id]}>X</button></td> 
            </tr>
          )}
        </tbody>
      </table>  
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : this.renderProfilesTable(this.state.profiles);

    return (
      <div>
            <h1 id="tabelLabel" >Profiles</h1>
            {this.state.isUpdated ? <h2 className="updated" style={{ visibility: 'visible' }}> Updated! </h2> : <h2 className="notUpdated" style={{visibility:'hidden'}}> hello friend, welcome to my code </h2>
  } 
            {this.state.isDeleted ? <h4 className="deleted" style={{ visibility: 'visible' }}> You have to delete the associated exercises first! </h4> : <h4 style={{ visibility: 'hidden' }}> enjoy your stay </h4>}
        {contents}
      </div>
    );
    }

  async populateProfileData() {
    const response = await fetch('api/profiles');
    const data = await response.json();
    this.setState({ profiles: data, loading: false });
    }
    deleteProfile(profile) {
        if (profile.sets == null || profile.sets.length == 0) {
            this.setState({ isDeleted: false, isUpdated: false });
            fetch('api/users/' + profile.user.id, {
                method: 'DELETE',
            }).then(res => res.text())
                .then(res => console.log(res)).then(s => {
                    this.setState({ isUpdated: false }); this.componentDidMount();
                });
        } else {
            this.setState({ isDeleted: true, isUpdated: false });
        }
       
    }
    updateProfile(profile) {
        console.log(profile);
        this.setState({ isDeleted: false });
        let prof = {
            id: profile.id, height: profile.height, weight: profile.weight, userId: profile.userId, disabilities: profile.disabilities,
            medicalConditions: profile.medicalConditions
        };
        fetch('api/profiles/' + prof.id, {
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(prof)
        }).then(s => {
            this.setState({ isUpdated: true }); this.componentDidMount();
    });
            }
}
