﻿import React, { Component } from 'react';
import { withRouter } from "react-router-dom";

export class AddExercise extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "", description: "", targetMuscleGroup: "", image: "", vidlink: "", exerciseRepetitions: "", workoutName: "",
            workoutType: "", complete: false, exercise: {}, set: {}, workout: {}, profileId: 0, profiles: [], loading: true
        };
        this.addExercise = this.addExercise.bind(this);
        this.addSet = this.addSet.bind(this);
        //this.addWorkout = this.addWorkout.bind(this);
    }
    componentDidMount() {
        this.populateProfileData();
    }


    async addSet() {
        let that = this.state;
        let set = {
            ExerciseRepetitions: that.exerciseRepetitions, ProfileId: that.profileId,
            ExerciseId: that.exercise.id
        }

        fetch('api/sets', {
            method: 'POST',
            body: JSON.stringify(set),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => {
            console.log(response); return response.json();
        }).then(json => {
            console.log(json);
            this.setState({
                set: json
            });
        }).then(s => { this.props.history.push("/fetch-exercise") });
    }


    /*async addWorkout() {
        let that = this.state;
        console.log(that.profileId);
        let workout = {
            Name: that.workoutName, Type: that.workoutType, Complete: that.complete,
        ProfileId: that.profileId}
        fetch('api/workouts', {
            method: 'POST',
            body: JSON.stringify(workout),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => {
            return response.json()
        }).then(json => {
            console.log(json);
            this.setState({
                workout: json
            });
        }).then(s => this.addSet());
    }*/

    async addExercise(event) {
        event.preventDefault();
        let that = this.state;
        let exercise = {
            Name: that.name, Description: that.description, TargetMuscleGroup: that.targetMuscleGroup, Image: that.image, VidLink: that.vidlink
        };
        fetch('api/exercises', {
            method: 'POST',
            body: JSON.stringify(exercise),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(response => {
            return response.json()
        }).then(res => {
            console.log(res);
            this.setState({
                exercise: res
            });
        }).then(s => this.addSet());
    }


    render() {
        if (!this.state.loading) {
            return (
                <div style={{ textAlign: "center" }}>
                    <h1>Add Exercise</h1>
                    <div style={{ textAlign: "center" }}>
                        <form onSubmit={this.addExercise}>
                            <label htmlFor="name">Exercise Name:
                    <input onChange={event => this.setState({ name: event.target.value })} type="text" id="name" name="name" required /><br /><br /></label>
                            <label htmlFor="description">Exercise Description:
                    <input onChange={event => this.setState({ description: event.target.value })} type="text" id="description" name="description" /><br /><br /> </label>
                            <label htmlFor="tmg">Target Muscle Group:
                    <input onChange={event => this.setState({ targetMuscleGroup: event.target.value })} type="text" id="tmg" name="tmg" required /><br /><br /> </label>
                            <label htmlFor="img"> Image Link:
                    <input onChange={event => this.setState({ image: event.target.value })} type="text" id="img" name="img"  /> <br /><br /> </label>
                            <label htmlFor="vidlink"> Video Link:
                    <input onChange={event => this.setState({ vidlink: event.target.value })} type="text" id="vidlink" name="vidlink" /> <br /><br /> </label>
                            <label htmlFor="rep"> Exercise Repetitions:
                    <input onChange={event => this.setState({ exerciseRepetitions: event.target.value })} type="number" id="rep" name="rep" required /> <br /><br /> </label>
                            <label htmlFor="profile"> Profile:
                                <select id="profile" required onChange={event => this.setState({ profileId: event.target.value })}>
                                    
                                    {this.state.profiles.map((profile) => {
                                        return (<option key={profile.id} value={profile.id} > {profile.user.first_Name + ' ' + profile.user.last_Name}</option>
                                    )})}</select></label>
                            <input type="submit" value="Submit" />
                        </form>
                    </div>


                </div>
            )
        } else {
            return (
                <div> Loading...</div>);
        }
    }
    async populateProfileData() {
        const response = await fetch('api/profiles');
        const data = await response.json();
        this.setState({ profiles: data, profileId: data[0].id, loading: false });
    }
}

export default withRouter(AddExercise);
